
#include <iostream> 
#include <conio.h> 


using namespace std;


float Add(float num1, float num2)
{
	// Math to add two numbers
	return num1 + num2;

}
float Subtract(float num1, float num2)
{
	// Math to subtract a number from another
	return num1 - num2;
}
float Multiply(float num1, float num2)
{
	// Math to multiply two numbers
	return num1 * num2;

}
bool Divide(float num1, float num2, float &answer)
{
	// Determine if user is trying to divide by 0
	if (num2 == 0)
		return false;

	// If the user isn't dividing by 0, perform math to divide
	answer = num1 / num2;
		return true;


}

int main()
{

	// Initialize variable for user input
	float userInput1;
	float userInput2;
	char op;
	float answer;
	char status;



	do {
		// Initial string displayed
		// Accept UserInput for variables
		cout << "\nEnter two positive numbers: \n";
		cin >> userInput1;
		cin >> userInput2;


		// Accept the type of Operand user wants to use
		cout << "\nEnter one of the following opererands: + , - , * , / \n";
		cin >> op;


		// Perform Math for users desired operand
		switch (op)
		{
		case('+'):
			cout << "\n= " << Add(userInput1, userInput2) << "\n";;
			break;
		case('-'):
			cout << "\n= " << Subtract(userInput1, userInput2) << "\n";;
			break;
		case('*'):
			cout << "\n= " << Multiply(userInput1, userInput2) << "\n";
			break;
		case('/'):
			if(Divide(userInput1, userInput2, answer) == true)
				cout << "\n= " << answer << "\n";
			else
				cout << "\nCannot divide by zero.";
			break;
		default: cout << "\nError: Operator is not correct.";
			break;
		}

		// Loop the calculator if user wants to do another calculation
		cout << "\nWould you like to perform another calculation? -Type (Y/N)?";
		cin >> status;
		} 
		while (status != 'N');
	
	

	(void)_getch;
	return 0;

}
